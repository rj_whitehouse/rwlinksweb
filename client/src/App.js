import React from 'react';
import logo from './logo.svg';
import LinkList from './Components/linkList';
import LinkForm from './Components/linkForm';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <LinkList />
      <LinkForm />
    </div>
  );
}

export default App;

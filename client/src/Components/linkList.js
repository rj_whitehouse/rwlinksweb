import React, { Component } from 'react';
import { proxyDomain } from '../Helpers/domain';

class LinkList extends Component {
	state = {
	  response: ''
	};
	
	componentDidMount() {
	  this.callApi()
		.then(res => this.setState({ response: res }))
		.catch(err => console.log(err));
	}
	
	callApi = async () => {
		console.log("calling api")
		var proxyPath = proxyDomain();
		console.log(proxyPath);
	
		const response = await fetch('/links');
		const body = await response.text();
		if (response.status !== 200) throw Error(body.message);	  
		return body;
	};
	
  render() {
	  console.log("inside render");
	  return (
		<div className="LinkList" dangerouslySetInnerHTML={createMarkup(this.state.response)}>
		</div>
	  );
	}
  }

  function createMarkup(stringData) {
	return {__html: stringData};
  }
  
  export default LinkList;
import React, { Component } from 'react';

class LinkForm extends Component {
	state = {
	  response: '',
	  linkTitle: '',
	  linkAddress: '',
	  responseToPost: '',
	};
	
	handleSubmit = async e => {
	  e.preventDefault();
	  const response = await fetch('/api/save', {
		method: 'POST',
		headers: {
		  'Content-Type': 'application/json',
		},
		body: JSON.stringify(
			{ 
				linkTitle: this.state.linkTitle, 
				linkAddress: this.state.linkAddress/*, 
				linkKey: 21 */
			}
		),
	  });
	  const body = await response.text();
	  
	  this.setState({ responseToPost: body });
	};
	
	render() {
		return (
			<div className="LinkForm">
				<form onSubmit={this.handleSubmit}>
					<p>
					<strong>Post to Server:</strong>
					</p>
					<input
					type="text"
					value={this.state.linkTitle}
					onChange={e => this.setState({ linkTitle: e.target.value })}
					/>
					<input
						type="text"
						value={this.state.linkAddress}
						onChange={e => this.setState({ linkAddress: e.target.value })}
					/>
					<button type="submit">Submit</button>
				</form>




				<p>{this.state.responseToPost}</p>
			</div>
		);
		}
	}
  
export default LinkForm;
/* This config file is only for transpiling the Express server file. */

const path = require('path');
const webpack = require('webpack');
const NodeExternals = require('webpack-node-externals');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = (env, argv) => {
	const { mode } = argv;
	const isDev = argv.mode === 'development';
	const isProd = argv.mode === 'production';

	const getPlugins = () => {
		const plugins = [
			new webpack.DefinePlugin({
				isDev: JSON.stringify(isDev),
				isProd: JSON.stringify(isProd),
				mode: JSON.stringify(mode),
			}),
		];
		if (isDev) {
			plugins.push(new NodemonPlugin());
		}
		return plugins;
	};

	return {
		entry: {
			server: './src/server.js',
		},
		externals: [NodeExternals()], // https://github.com/webpack/webpack/issues/1576
		mode,
		module: {
			rules: [
				{
					// Transpiles ES6-8 into ES5
					test: /\.jsx?$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
					},
				},
				{
					test: /\.(html)$/,
					use: {
						loader: 'html-loader',
						options: {},
					},
				},
				{
					test: /\.svg$/,
					use: 'url-loader',
				}
			],
		},
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname, '../dist'),
		},
		plugins: getPlugins(),
		resolve: {
			extensions: ['.js', '.jsx'],
			alias: {
				clientLink: path.resolve(__dirname, './../client/'),
				serverLink: path.resolve(__dirname, './../server/')
			}
		},
		stats: isDev
			? {
				all: false,
				assets: true,
				builtAt: true,
			  }
			: 'normal',
		target: 'node',
		watch: isDev,
		node: {
			__dirname: true
		}
	};
};

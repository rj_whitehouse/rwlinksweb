import React from 'react';
import containerStyles from '../styles/listLinkHeaderStyles';

const LinkListEditForm = props => {
   
    let outputText = "This is the create new link form";
    let linkTitle = "";
    if (props.data) {
        outputText = "This is the edit link form";

        console.log(props.data);
        console.log(props.data.links);

        props.data.links.map((link) => {
            console.log("link data found");
            console.log(link);
            linkTitle = link.LinkTitle;
        });

        console.log(linkTitle);
    }

    return (<React.Fragment>
        <style>{containerStyles}</style>
        <div className="links__form">
            {outputText}
            <form>
                <label>Title: <input type="text" name="title" value={linkTitle} /></label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    </React.Fragment>);
};

export default LinkListEditForm;

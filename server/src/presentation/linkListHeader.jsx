import React from 'react';
import containerStyles from '../styles/listLinkHeaderStyles';

const LinkListHeader = props => {
   
    return (<React.Fragment>
        <style>{containerStyles}</style>
        <div className="links__header">
            <a href="links/edit/0">Add New Link</a>
        </div>
    </React.Fragment>);
};

export default LinkListHeader;

import React from 'react';
import prototypeStyles from '../styles/listLinkStyles';

const propTypes = {};
const defaultProps = {};

const LinkList = props => {
   
    if (props.data && props.data.links && Array.isArray(props.data.links)) {
        var counter = 0;
        let outputData = props.data.links.map((link) => {
            counter = counter + 1;
            var stringReturn = "",
                linkLink = "/links/" + link.linkKey;

            stringReturn = (
                <div className="linkRow" key={link.linkKey}>
                    <div className="linkRowItem linkRow-linkCounter">
                        {counter}
                    </div>
                    <div className="linkRowItem linkRow-linkTitle">
                        <a
                            className="footer-accordion__list-link"
                            href={linkLink}
                        >
                            {link.LinkTitle}
                        </a>
                    </div>
                    <div className="linkRowItem linkRow-linkVisits">
                        {link.LinkVisits}
                    </div>
                </div>);
            return (stringReturn);
        })
        return (<React.Fragment>
            <style>{prototypeStyles}</style>
            <div className="links__wrapper">
                {outputData}
            </div>
        </React.Fragment>);
      } else {
        return <p>Loading Links</p>
      }

	
};

LinkList.propTypes = propTypes;
LinkList.defaultProps = defaultProps;

export default LinkList;

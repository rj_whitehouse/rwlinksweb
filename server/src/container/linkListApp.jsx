import React from 'react';
import LinkList from '../presentation/LinkList';
import LinkListHeader from '../presentation/linkListHeader';

const LinkListApp = props => {
    return (
        <div>
            <LinkListHeader />
            <LinkList {...props} />
        </div>
    );
};

/*App.propTypes = propTypes;
App.defaultProps = defaultProps;*/

export default LinkListApp;

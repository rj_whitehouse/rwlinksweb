// yarn run build:server
// yar server

// yarn run start

import LinkListApp from './container/linkListApp';
import LinkListEditForm from './presentation/linkListEditForm';
import compression from "compression";
import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { ServerStyleSheet } from 'styled-components';
import Axios from 'axios';

const express = require('express');
const path = require('path');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const isDev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 5000;

// app.use(express.static(path.resolve(__dirname, '../client/build')));

// Multi-process to utilize all CPU cores.
if (!isDev && cluster.isMaster) {
    console.error(`Node cluster master ${process.pid} is running`);
  
    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }
  
    cluster.on('exit', (worker, code, signal) => {
      console.error(`Node cluster worker ${worker.process.pid} exited: code ${code}, signal ${signal}`);
    });
  
} else {
    const app = express();

    // Priority serve any static files.
    console.log(path.resolve(__dirname, '../react-ui/build'));
    app.use(express.static(path.resolve(__dirname, '../client/build')));
    console.log("next stuff");

    /* app.use(compression());
    app.use(express.static("public"));
    app.use(express.json()); */

    var responseSent = false;

    app.post('/api/save', (req, res) => {
        console.log(req.body);
        console.log(req.body.linkTitle);

        var dataToPost = { linkKey: 0,
            linkTitle: req.body.linkTitle,
            linkAddress: req.body.linkAddress 
        }

        var authOptions = {
            method: 'POST',
            url: 'http://rwlinks.richard-whitehouse.me/v1/link/PostLink',
            // url: 'http://rwlinks.main/v1/link/PostLink',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(JSON.stringify(dataToPost))
        };

        console.log("The auth options are:");
        console.log(authOptions);

        Axios(authOptions)
        .then((response) => {
            console.log(response);
            res.send("Done!");
        })
        .catch((err) => {
            if (err) {
                console.log(err);
                res.status(404).send();
            }
        });
        responseSent = true;
    });

    app.get('/links', (req, res) => {
        console.log("getting links");
        Axios.get('http://rwlinks.richard-whitehouse.me/v1/link/GetLinks')
        .then((response) => {
            const { data } = response;
            const linksStylesheet = new ServerStyleSheet();
            const applicationHTML = renderToStaticMarkup(linksStylesheet.collectStyles(<LinkListApp data = {data} />));
            res.set('Content-Type', 'application/json');
            res.send(applicationHTML);
            // res.send({data});
            
            // const css = "";
            // const fragmentHTML = Fragment(css, Manifest, applicationHTML, {}, isDev);
        })
        .catch((err) => {
            if (err) {
                console.log(err);
                res.status(404).send();
            }
        })
        responseSent = true;
    });
    app.get('/links/:linkKey', (req, res) => {
        console.log("getting link item");
        if (req.params.linkKey && req.params.linkKey != 0) {
            Axios.get('http://rwlinks.richard-whitehouse.me/v1/link/VisitLink/' + req.params.linkKey)
            .then((response) => {
                const { data } = response;

                let outputData = data.links.map((link) => {
                    return(link.LinkAddress);
                });

                res.writeHead(301, { "Location": outputData });
                res.set('Content-Type', 'application/json');
                res.end();
            })
            .catch((err) => {
                if (err) {
                    console.log(err);
                    res.status(404).send();
                }
            })
        }
        responseSent = true;
    });
    app.get('/links/edit/:linkKey', (req, res) => {
        console.log("getting edit linkkey");
        let data; 
        if (req.params.linkKey && req.params.linkKey != 0) {
            data = Axios.get('http://rwlinks.richard-whitehouse.me/v1/link/VisitLink/' + req.params.linkKey)
            .then((response) => {
                const { data } = response;

                let outputData = data.links.map((link) => {
                    console.log("link data found");
                    console.log(link);
                    return(link.LinkAddress);
                });

                outputEditForm(data, res);

                // res.writeHead(301, { "Location": outputData });
                // res.end();
            })
            .catch((err) => {
                if (err) {
                    console.log(err);
                    res.status(404).send();
                }
            })
        } else {
            outputEditForm (data, res);
        }
        responseSent = true;
    });

    // All remaining requests return the React app, so it can handle routing.
    app.get('*', function(request, response) {
        console.log("sending other stuff back");
        console.log(request.path);
        if (!responseSent) {
            console.log(__dirname + " is the dir name");
            console.log("This is about to error as we don't have index.html to return");
            response.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
        } else {
            console.log("a response has been sent so don't need to do anything else");
        }
    });

    app.listen(PORT, function () {
        console.error(`Node ${isDev ? 'dev server' : 'cluster worker '+process.pid}: listening on port ${PORT}`);
    });

    console.log("moving on");

    function outputEditForm(data, res) {
        console.log("show edit form");
        const editFormStylesheet = new ServerStyleSheet();
        const applicationHTML = renderToString(editFormStylesheet.collectStyles(<LinkListEditForm data = {data} />));
        // const css = "";
        // const fragmentHTML = Fragment(css, Manifest, applicationHTML, {}, isDev);
        res.send(applicationHTML);
    }
}
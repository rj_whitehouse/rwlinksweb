export default `

.linkRow {
    border-bottom: 1px solid gray;
    width: 30%;
    clear: both;
    float: left
}
.linkRow div {
    float: left;
    padding: 3px;
}
.linkRow-linkCounter {
    width: 40px;
}
.linkRow-linkTitle {
    width: 70%;
}`